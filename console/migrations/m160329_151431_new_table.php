<?php

use yii\db\Schema;
use yii\db\Migration;

class m160329_151431_new_table extends Migration
{
    public function up()
    {
         $this->createTable('{{%category_item}}', [
            'id' => Schema::TYPE_PK,
            'category_id' => Schema::TYPE_INTEGER,
            'product_id' => Schema::TYPE_INTEGER,
        ], $tableOptions);

        $this->addForeignKey('fk-category_item-category_id-category-id', '{{%category_item}}', 'category_id', '{{%category}}', 'id', 'SET NULL');
        $this->addForeignKey('fk-category_item-product_id-product-id', '{{%order_item}}', 'product_id', '{{%product}}', 'id', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable('{{%category_item}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
